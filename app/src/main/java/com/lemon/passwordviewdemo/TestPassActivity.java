package com.lemon.passwordviewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.lemon.password.PasswordView;

import java.util.Objects;

public class TestPassActivity extends AppCompatActivity {
    private String tag = getClass().getSimpleName();
    private PasswordView pwvInput;
    private PasswordView pwvConfirm;
    private Button btnChangeMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_pass);

        pwvInput = (PasswordView) findViewById(R.id.pwvInput);
        pwvConfirm = (PasswordView) findViewById(R.id.pwvConfirm);

        pwvConfirm.setPasswordListener(new PasswordView.PasswordListener() {
            @Override
            public void passwordChange(String changeText) {
                Log.d(tag, "changeText = " + changeText);
            }

            @Override
            public void passwordComplete() {
                Log.d(tag, "passwordComplete");

            }

            @Override
            public void keyEnterPress(String password, boolean isComplete) {
                Log.d(tag, "password = " + password + " isComplete = " + isComplete);
                if (!Objects.equals(password, pwvInput.getPassword())) {
                    Toast.makeText(TestPassActivity.this, "两次密码输入不一致!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TestPassActivity.this, "设置成功!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        (((CheckBox) findViewById(R.id.cbShowNumber))).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pwvInput.setCipherEnable(!isChecked);
                pwvConfirm.setCipherEnable(!isChecked);
            }
        });
    }
}